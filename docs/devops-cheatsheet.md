## Networking

```bash
export PUBLIC_IP=83.212.74.231
export PUBLIC_IF=eth2
echo 200 isp2 >> /etc/iproute2/rt_tables
ip route add default via $PUBLIC_IP dev $PUBLIC_IF table isp2
ip rule add from $PUBLIC_IP table isp2
ip rule add from $PUBLIC_IP to 10.0.0.0/8 lookup main
```

## Object store

Get the access/secret key from ceph rgw

```bash
echo $(kubectl -n rook-ceph get secret -l user=my-user -o jsonpath='{ .items[0].data.AccessKey }' | base64 --decode)
echo $(kubectl -n rook-ceph get secret -l user=my-user -o jsonpath='{ .items[0].data.SecretKey }' | base64 --decode)
```


Get minio client
```bash
curl https://dl.minio.io/client/mc/release/linux-amd64/mc -s -o mc
```

Mirror a bucket to a different system
```bash
./mc config add host h1 http://<h1> <a1> <s1> --api S3v4
./mc config add host h2 http://<h2> <a2> <s2> --api S3v4
./mc mb h2/b2
./mc --mirror h1/b1 h2/b2 
```

## Rook and Ceph

Start rook toolbox:
```bash
kubectl apply -f ./rook/cluster/examples/kubernetes/ceph/toolbox.yaml
```

Get shell access to rook toolbox:
```bash
kubectl -n rook-ceph exec -it $(kubectl -n rook-ceph get pod -l "app=rook-ceph-tools" -o jsonpath='{.items[0].metadata.name}') bash
```

Check ceph space consumption:
```bash
ceph df
```

### Block devices

Check
```bash
rbd ls replicapool
```
Then, you can try to delete a block using
```bash
rbd rm replicapool/block-name
```
or everything
```bash
rbd ls replicapool | xargs -I {} rbd rm replicapool/{}
```

if there are watchers in a block you cannot delete the block device.
to see if anyone is watching this device do:
```bash
rbd status replicapool/block-name
```
To force the watcher out, find its ip and do:
```bash
ceph osd blacklist add <ip>
```
Now you can delete the block you want.
Remember to clear the blacklist using
```bash
ceph osd blacklist clear 
```

### Object store

To see if there are any object to garbage collect:
```bash
rados-admin gc ls --include-all
```

To enforce the garbage collection:
```bash
rados-admin gc process --include-all
```
